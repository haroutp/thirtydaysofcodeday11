﻿using System;

namespace Day11
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arr = new int[6][];

            for (int i = 0; i < 6; i++) {
                arr[i] = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp));
            }

            int R = arr.Length;
            int C = arr[0].Length;
            int maxHourGlass = int.MinValue;

            for (int i = 0; i < R - 2; i++)
            {
                for (int j = 0; j < C - 2; j++)
                {
                    int sum = (arr[i][j] + arr[i][j + 1] + arr[i][j + 2]) +
                     (arr[i + 1][j + 1]) +
                      (arr[i + 2][j] + arr[i+ 2][j + 1] + arr[i + 2][j + 2]);

                    if (maxHourGlass < sum){
                        maxHourGlass = sum;
                    }
                }
            }
            System.Console.WriteLine(maxHourGlass);
        }
    }
}
